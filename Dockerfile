# Use the official OpenJDK base image
FROM openjdk:23-slim-bullseye

# Set the working directory inside the container
WORKDIR /app

# Copy the JAR file into the container at /app
COPY target/EventAPI-0.0.1.jar /app/EventAPI-0.0.1.jar

# Expose the port that the application will run on
EXPOSE 8081

# Define environment variables
ENV SERVER_PORT=8081
ENV SPRING_DATASOURCE_URL=jdbc:postgresql://eventdatabase:5432/mojay_events
ENV SPRING_DATASOURCE_USERNAME=event_db
ENV SPRING_DATASOURCE_PASSWORD=1234
ENV SPRING_JPA_HIBERNATE_DDL_AUTO=create-drop

# Command to run your application
CMD ["java", "-jar", "EventAPI-0.0.1.jar"]

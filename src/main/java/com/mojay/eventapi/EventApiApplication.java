package com.mojay.eventapi;

import com.mojay.eventapi.models.Event;
import com.mojay.eventapi.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;

@SpringBootApplication
public class EventApiApplication implements CommandLineRunner {
	@Autowired
	EventRepository eventRepository;

	public static void main(String[] args) {
		SpringApplication.run(EventApiApplication.class, args);
	}
	@Override
	public void run(String... args) {
		eventRepository.save(new Event("Adele Concert", Date.valueOf(LocalDate.now()), Time.valueOf(LocalTime.now()),
				"36.565656", "52.679170", "Babol Confectionery", "19.99"));
		eventRepository.save(new Event("Food Party", Date.valueOf(LocalDate.now()), Time.valueOf(LocalTime.now()),
				"36.559697", "52.679008", "KFC Babol", "39.99"));
		eventRepository.save(new Event("Disco Night", Date.valueOf(LocalDate.now()), Time.valueOf(LocalTime.now()),
				"36.561998", "52.684607", "Khabgah Aminian", "0.00"));
	}
}

package com.mojay.eventapi.models;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.sql.Date;
import java.sql.Time;

@Entity
@Data
@RequiredArgsConstructor
@NoArgsConstructor
@Table(name = "events")
public class Event {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    @Column(name = "name")
    @NonNull
    private String name;
    @Column(name = "date")
    @NonNull
    private Date date;
    @Column(name = "time")
    @NonNull
    private Time time;
    @Column(name = "lat")
    @NonNull
    private String lat;
    @Column(name = "lng")
    @NonNull
    private String lng;
    @Column(name = "venueName")
    @NonNull
    private String venueName;
    @Column(name = "price")
    @NonNull
    private String price;

}